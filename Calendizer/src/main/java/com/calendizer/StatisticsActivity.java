package com.calendizer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import android.support.v4.app.Fragment;

public class StatisticsActivity extends CoreActivity {

    private SQLiteDatabase database;
    private CharSequence[] entries;
    private int[][] numbers;
    private final int maxTilesCount = 5;
    private CalendizerSQLiteOpenHelper sqLiteOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);
        int i = 0;
        //List<int[]> numberz = new ArrayList<int[]>();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        int tilesCount = Integer.parseInt(sharedPref.getString("tilesCount", "3"));
        //numbers = new int[tilesCount][];
        for (i = 0; i < tilesCount; i++) {
            //holder.tileName[i].setText(sharedPref.getString("tile_name_" + (i + 1), ""));
            String key = "TileNameView" + (i + 1);
            int resID = getResources().getIdentifier(key, "id", getPackageName());
            TextView t = (TextView) findViewById(resID);
            t.setText(sharedPref.getString("tile_name_" + (i + 1), ""));
            loadTilesNumbers(i);
            //numbers[i] = loadTilesNumbers(i);
            //numberz.add(loadTilesNumbers(i));
        }
        for (; i < 5; i++) {
            String key = "linearLayout" + (i);
            int resID = getResources().getIdentifier(key, "id", getPackageName());
            LinearLayout t = (LinearLayout) findViewById(resID);
            t.setVisibility(View.GONE);
        }


        /////////////////////////////////
        //bez wykresu rocznego jak na razie
        for (int j = 0; j < 5; j++) {
            String key = "WykRo" + (j + 1);
            int resID = getResources().getIdentifier(key, "id", getPackageName());
            RelativeLayout t = (RelativeLayout) findViewById(resID);
            t.setVisibility(View.GONE);
            key = "PlotMies" + (j + 1);
            resID = getResources().getIdentifier(key, "id", getPackageName());
            TextView v = (TextView) findViewById(resID);
            v.setText("Wykres");
        }


        //numbers = loadTilesNumbers();

        //TextView t1 = (TextView)findViewById(R.id.TileNameView1);

//        for (int i=0; i<maxTilesCount; i++){
//            String key = "TileNameView"+(i+1);
//            int resID = getResources().getIdentifier(key, "id", getPackageName());
//            TextView t = (TextView)findViewById(resID);
//            t.setText(entries[i]);
//            int j = i;
//            int k = i;
//        }

    }

    public int[] loadTilesNumbers(int position) {
        //String[] columns = {"_id", "name"};
        //Cursor cursor = database.query("tilesnames", columns, null, null, null, null, null);
        //Cursor cursor = database.rawQuery("Select * from tilesnames", null);
        sqLiteOpenHelper = new CalendizerSQLiteOpenHelper(this);
        database = sqLiteOpenHelper.getReadableDatabase();
        String[] columns = {"_id", "value", "date"};
        String selection = "position=?";
        String[] selectionArgs = {Integer.toString(position)};
        Cursor cursor = database.query("tiles", columns, selection, selectionArgs, null, null, null);
        //if(cursor==null)return null;
        int count = cursor.getCount();
        if (count == 0) return null;
        cursor.moveToFirst();
        //CharSequence[] names = new CharSequence[count];
        int[] numbers = new int[count];
        int min, max, sum = 0, curV;
        double avg = 0;
        min = max = cursor.getInt(cursor.getColumnIndex("value"));
        for (int i = 0; i < count; i++) {
            curV = numbers[i] = cursor.getInt(cursor.getColumnIndex("value"));
            if (curV > max)
                max = curV;
            if (curV < min)
                min = curV;
            sum += curV;
            cursor.moveToNext();
        }
        if (count != 0)
            avg = 1.0d * sum / count;

        String key = "avgValue" + (position + 1);
        int resID = getResources().getIdentifier(key, "id", getPackageName());
        TextView t = (TextView) findViewById(resID);
        //t.setText(Double.toString(avg));
        t.setText(String.format("%.2f", avg));


        key = "minValue" + (position + 1);
        resID = getResources().getIdentifier(key, "id", getPackageName());
        t = (TextView) findViewById(resID);
        t.setText(Integer.toString(min));

        key = "maxValue" + (position + 1);
        resID = getResources().getIdentifier(key, "id", getPackageName());
        t = (TextView) findViewById(resID);
        t.setText(Integer.toString(max));


        cursor.close();
        database.close();
        return numbers;
    }


    public void drawPlot(View v) {
        int tag = Integer.parseInt(v.getTag().toString());
        int tilePosition = tag / 10 - 1;
        int czyMiesieczny = tag % 10; //1 - miesieczny, 2 - roczny

        Intent i = new Intent(getApplicationContext(), PlotActivity.class);
        i.putExtra("TILEPOSITION", tilePosition);
        i.putExtra("CZYMIESIECZNY", czyMiesieczny);
        //i.putExtra("YEAR", mYear);
        startActivity(i);
    }


//    db = sqlHelper.getWritableDatabase();
//    String[] columns = {"_id", "value", "position"};
//    String selection = "date=?";
//    String[] selectionArgs = {date};
//    cursor = db.query("tiles", columns, selection, selectionArgs, null, null, null);
//    cursor.moveToFirst();
//    for (int i = 0; i < cursor.getCount(); i++) {
//        int position = cursor.getInt(cursor.getColumnIndex("position"));
//        tiles[position].setId(cursor.getInt(cursor.getColumnIndex("_id")));
//        tiles[position].setValue(cursor.getInt(cursor.getColumnIndex("value")));
//        tiles[position].setPosition(position);
//        cursor.moveToNext();
//    }
//    cursor.close();
//    db.close();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //menu.removeItem(R.id.action_settings);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        switch (item.getItemId()) {
//            case R.id.action_settings:
//                onClickMenuShowSettings(item);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//    void onClickMenuShowSettings(MenuItem item) {
//        Intent intent = new Intent(this, Settings.class);
//        startActivity(intent);
//    }

    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            return rootView;
//        }
//    }
//
}
