package com.calendizer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amonite on 18.10.13.
 */
public class Day {
    private int id;
    private int dayNumber;
    private int month;
    private int year;
    private List<Task> tasks;
    private Tile[] tiles;
    private SQLiteDatabase db;
    private String date;
    private Cursor cursor;
    private CalendizerSQLiteOpenHelper sqlHelper;

    public Day(int dayNumber, int month, int year, CalendizerSQLiteOpenHelper sqlHelper, Context context) {
        this.setDayNumber(dayNumber);
        this.setMonthName(month);
        this.setYear(year);
        this.sqlHelper = sqlHelper;
        date = year + "-" + month + "-" + dayNumber;

        tiles = new Tile[5];
        for (int i = 0; i < 5; i++) {
            tiles[i] = new Tile();
        }

        tasks = new ArrayList<Task>();
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public Tile[] getTiles() {
        return tiles;

    }

    public void setTiles(Tile[] tiles) {
        this.tiles = tiles;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(int dayNumber) {
        this.dayNumber = dayNumber;
    }

    public int getMonthName() {
        return month;
    }

    public void setMonthName(int month) {
        this.month = month;
    }

    public void getTasksSQL() {
        db = sqlHelper.getWritableDatabase();
        String[] columns = {"_id", "value"};
        String selection = "date=?";
        String[] selectionArgs = {date};
        cursor = db.query("tasks", columns, selection, selectionArgs, null, null, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Task task = new Task(cursor.getString(cursor.getColumnIndex("value")));
            task.setId(cursor.getInt(cursor.getColumnIndex("_id")));
            tasks.add(i, task);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();
    }

    public void getTilesSQL() {
        db = sqlHelper.getWritableDatabase();
        String[] columns = {"_id", "value", "position"};
        String selection = "date=?";
        String[] selectionArgs = {date};
        cursor = db.query("tiles", columns, selection, selectionArgs, null, null, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            int position = cursor.getInt(cursor.getColumnIndex("position"));
            tiles[position].setId(cursor.getInt(cursor.getColumnIndex("_id")));
            tiles[position].setValue(cursor.getInt(cursor.getColumnIndex("value")));
            tiles[position].setPosition(position);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
    }

    public void updateTask(int id, String value) {
        db = sqlHelper.getWritableDatabase();
        for (Task t : tasks) {
            if (t.getId() == id) {
                t.setValue(value);
                break;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", value);
        String whereClause = "_id=?";
        String[] args = new String[]{Integer.toString(id)};
        db.update("tasks", contentValues, whereClause, args);
        db.close();
    }

    public void deleteTask(int id) {
        db = sqlHelper.getWritableDatabase();
        for (Task t : tasks) {
            if (t.getId() == id) {
                tasks.remove(t);
                break;
            }
        }

        String whereClause = "_id=?";
        String[] args = new String[]{Integer.toString(id)};
        int d = db.delete("tasks", whereClause, args);
        Log.d("DELETE", "deleteTask deleteTask deleteTask: " + d);
        db.close();
    }

    public void insertTask(String value) {
        db = sqlHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", value);
        contentValues.put("date", date);
        long d = db.insert("tasks", null, contentValues);
        Task task = new Task(value);
        task.setId((int) d);
        tasks.add(task);
        db.close();
    }

    public void insertTile(int value, int position) {
        db = sqlHelper.getWritableDatabase();
        String name = tiles[position].getName();
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", value);
        contentValues.put("position", position);
        contentValues.put("date", date);
        long d = db.insert("tiles", null, contentValues);
        tiles[position].setId((int) d);
        tiles[position].setValue(value);
        tiles[position].setPosition(position);
        db.close();
    }

    public void updateTile(int value, int position) {
        db = sqlHelper.getWritableDatabase();
        int id = tiles[position].getId();
        tiles[position].setValue(value);
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", value);
        String whereClause = "_id=?";
        String[] args = new String[]{Integer.toString(id)};
        db.update("tiles", contentValues, whereClause, args);
        db.close();
    }
}
