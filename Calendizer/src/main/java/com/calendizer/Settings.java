package com.calendizer;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.util.UUID;

public class Settings extends CoreActivity implements View.OnClickListener, EditTilesFragment.OnTileNameChangedListener {
    public static CalendizerSQLiteOpenHelper sqlHelper;
    public String user = "test";
    public String pass = "test";
    private String url = "http://www.amonite.comuv.com/new.php";
    private String authString = user + ":" + pass;
    private String path = "/data/data/com.calendizer/databases/Calendizer";
    private String file_url;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sqlHelper = new CalendizerSQLiteOpenHelper(this);
        setContentView(R.layout.settings);

        Button uploadButton = (Button) findViewById(R.id.uploadButton);
        Button downloadButton = (Button) findViewById(R.id.downloadButton);
        uploadButton.setOnClickListener(this);
        downloadButton.setOnClickListener(this);

        File direct = new File(Environment.getExternalStorageDirectory() + "/database");
        if (!direct.exists()) {
            direct.mkdir();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menu.removeItem(R.id.action_settings);
        return true;
    }

    private void postFile() {
        try {
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url);

            File file = new File(path);
            FileBody fileBody = new FileBody(file);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", fileBody);
            builder.addTextBody("user", user);
            builder.addTextBody("pass", pass);
            builder.addTextBody("WWW-Authenticate: Basic realm", Base64.encodeToString(authString.getBytes(), Base64.NO_WRAP));
            builder.addTextBody("id", getPhoneId());
            httpPost.setEntity(builder.build());

            // execute HTTP post request
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {

                String responseStr = EntityUtils.toString(resEntity).trim();
                Log.v("XXX", "Response: " + responseStr);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private String getPhoneId() {
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }

    private void importDB() {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = path;
                String backupDBPath = "/database/Calendizer";
                File backupDB = new File(currentDBPath);
                File currentDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                currentDB.delete();

                MyPreferenceFragment preferenceFragment = (MyPreferenceFragment) getFragmentManager().findFragmentById(R.id.prefernce_fragment);

                if (preferenceFragment != null) {
                    preferenceFragment.setDownloadedSummaries();
                }

                Toast.makeText(getBaseContext(), "Zakonczono pobieranie", Toast.LENGTH_LONG)
                        .show();
            }
        } catch (Exception e) {

            Toast.makeText(getBaseContext(), "Brak danych do pobrania", Toast.LENGTH_LONG)
                    .show();

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.uploadButton:
                new Connection().execute();
                break;
            case R.id.downloadButton:
                file_url = "http://www.amonite.comuv.com/uploads/" + getPhoneId();
                new DownloadFileFromURL().execute(file_url);
                break;
        }
    }

    @Override
    public void OnTileNameAdded() {
        MyPreferenceFragment preferenceFragment = (MyPreferenceFragment) getFragmentManager().findFragmentById(R.id.prefernce_fragment);

        if (preferenceFragment != null) {
            preferenceFragment.setPreferences();
        }
    }

    private class Connection extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                postFile();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = ProgressDialog.show(Settings.this, "Wysylanie", "", true);
        }


        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            Toast.makeText(getBaseContext(), "Zakonczono wysylanie", Toast.LENGTH_LONG)
                    .show();
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = ProgressDialog.show(Settings.this, "pobieranie", "", true);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();

                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream("/sdcard/database/Calendizer");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", "" + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            importDB();
        }

    }
}
