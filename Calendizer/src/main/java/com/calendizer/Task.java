package com.calendizer;

/**
 * Created by Amonite on 26.10.13.
 */
public class Task {
    private int id;
    private String value;

    public Task(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
