package com.calendizer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

public class CoreActivity extends Activity {
    public static CalendizerSQLiteOpenHelper sqLiteOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sqLiteOpenHelper = new CalendizerSQLiteOpenHelper(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;

        int id = item.getItemId();
        switch (id) {
//            case R.id.action_monthView:
//                onClickMenuShowMonthView(item);
////                finish();
//                break;
//            case R.id.action_dayView:
//                onClickMenuShowDayView(item);
////                if (!getClass().getSimpleName().equals("MonthActivity")) {
////                    finish();
////                }
//                break;
            case R.id.action_statView:
                onClickMenuShowStatActivity(item);
                if (!getClass().getSimpleName().equals("MonthActivity") && !getClass().getSimpleName().equals("MainActivity")) {
                    finish();
                }
                break;
            case R.id.action_settings:
                onClickMenuShowSettings(item);
                if (!getClass().getSimpleName().equals("MonthActivity") && !getClass().getSimpleName().equals("MainActivity")) {
                    finish();
                }
                break;
            default:
                handled = super.onOptionsItemSelected(item);
        }

        return handled;
    }

//    void onClickMenuShowMonthView(MenuItem item) {
//        Intent intent = new Intent(this, MonthActivity.class);
//        startActivity(intent);
//    }

    void onClickMenuShowSettings(MenuItem item) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }
//
//    void onClickMenuShowDayView(MenuItem item) {
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
//    }

    void onClickMenuShowStatActivity(MenuItem item) {
        Intent intent = new Intent(this, StatisticsActivity.class);
        startActivity(intent);
    }
}
