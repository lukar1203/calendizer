package com.calendizer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends CoreActivity {
    public static DayArrayAdapter adapter;
    private ArrayList<Day> days;
    private ListView listView;
    private int month;
    private int year;
    private int day;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //menu.removeItem(R.id.action_dayView);
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        day = 21;
        month = 9;
        year = 2013;
        //--------------------------------------------------
        //Odbieram dane z MonthView:
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            day = extras.getInt("DAY");
            month = extras.getInt("MONTH");
            year = extras.getInt("YEAR");
        }
        //--------------------------------------------------
        setContentView(R.layout.main_activity);
        listView = (ListView) findViewById(R.id.dayListView);
        // Numeracja miesiecy od 0 do 11
        days = GetListOFDays(month, year);
        // use your own layout
        adapter = new DayArrayAdapter(this,
                R.layout.day, days);
        listView.setAdapter(adapter);

        // Wybor wybranej pozycji na liscie. Aby umiescic dzien na srodkowej pozycji: nrDnia - 2
        listView.setSelection(day - 2);

    }

    private ArrayList<Day> GetListOFDays(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);

        int numberOfDays = calendar.getActualMaximum(Calendar.DATE);
        ArrayList<Day> list = new ArrayList<Day>();
        //String monthName = new SimpleDateFormat("MMM").format(calendar.getTime());

        for (int i = 1; i <= numberOfDays; i++) {
            Day day = new Day(i, month, year, sqLiteOpenHelper, this);
            day.getTasksSQL();
            day.getTilesSQL();
            list.add(day);
        }

        return list;
    }

    public void changeTask(View v) {
        // Reakcja na klikniecie 1 zadania

        final EditText dialogEditText = new EditText(this);
        final Day day = adapter.getItem(listView.getPositionForView(v));
        final int tag = Integer.parseInt(v.getTag().toString());

        dialogEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(80)});
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Zmien zadanie");
        dialogEditText.setText(((TextView) v).getText());
        builder.setView(dialogEditText);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String task = dialogEditText.getText().toString();
                if (task != null && !task.isEmpty()) {
                    //day.setMonthName(task);
                    day.updateTask(tag, task);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                day.deleteTask(tag);
                adapter.notifyDataSetChanged();
            }
        });
        builder.show();
    }

    public void addTask(View v) {

        final EditText dialogEditText = new EditText(this);
        final Day day = adapter.getItem(listView.getPositionForView(v));

        if (day.getTasks().size() < 4) {
            dialogEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(80)});
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Dodaj zadanie");
            builder.setMessage(null);
            builder.setView(dialogEditText);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String task = dialogEditText.getText().toString();
                    if (task != null && !task.isEmpty()) {
                        //day.setMonthName(task);
                        day.insertTask(task);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
            builder.show();
        }
    }

    public void editTile(View v) {
        final EditText dialogEditText = new EditText(this);
        final Day day = adapter.getItem(listView.getPositionForView(v));
        final int tag = Integer.parseInt(v.getTag().toString()) - 1;
        dialogEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialogEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Zmien wartosc");
        //dialogEditText.setText(((TextView)( (ViewGroup)v).getChildAt(tag)).getText());
        builder.setView(dialogEditText);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String text = dialogEditText.getText().toString();
                if (!text.equals("")) {
                    int value = Integer.parseInt(text);
                    //day.setMonthName(task);
                    if (day.getTiles()[tag].getValue() != 0)
                        day.updateTile(value, tag);
                    else {
                        day.insertTile(value, tag);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        days = GetListOFDays(month, year);
        // use your own layout
        adapter = new DayArrayAdapter(this,
                R.layout.day, days);
        listView.setAdapter(adapter);
        listView.setSelection(day - 2);
        adapter.notifyDataSetChanged();
    }
}