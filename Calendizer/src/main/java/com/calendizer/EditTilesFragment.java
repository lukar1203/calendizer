package com.calendizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by Amonite on 15.11.13.
 */
public class EditTilesFragment extends Fragment implements View.OnClickListener {
    private CalendizerSQLiteOpenHelper sqLiteOpenHelper;
    private SimpleCursorAdapter cursorAdapter;
    private SQLiteDatabase database;
    private ListView listView;
    private OnTileNameChangedListener mCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sqLiteOpenHelper = ((Settings) getActivity()).sqLiteOpenHelper;

        View view = inflater.inflate(R.layout.edit_tiles, container, false);
        displayListView(view);


        Button b = (Button) view.findViewById(R.id.button);
        b.setOnClickListener(this);

        return view;
    }

    private void displayListView(View view) {
        database = sqLiteOpenHelper.getWritableDatabase();
        String[] columns = {"_id", "name"};
        Cursor cursor = database.query("tilesnames", columns, null, null, null, null, null);
        //Cursor cursor = database.rawQuery("Select * from tilesnames", null);
        cursor.moveToFirst();
        cursorAdapter = new SimpleCursorAdapter(getActivity(), R.layout.tile_name, cursor, columns, new int[]{R.id.tilesIdTextView, R.id.tileNameTextView}, 0);

        listView = (ListView) view.findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                editTileName(view);
            }
        });
        database.close();
    }

    public void addTileName(View view) {
        database = sqLiteOpenHelper.getWritableDatabase();
        EditText editTextView = (EditText) getView().findViewById(R.id.nameTextView);
        String name = editTextView.getText().toString();
        if (!name.isEmpty()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", editTextView.getText().toString());
            long d = database.insert("tilesnames", null, contentValues);
            editTextView.setText(null);
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editTextView.getWindowToken(), 0);
            updateCursor();
        }
        database.close();
    }

    private void updateCursor() {
        database = sqLiteOpenHelper.getWritableDatabase();
        String[] columns = {"_id", "name"};
        cursorAdapter.changeCursor(database.query("tilesnames", columns, null, null, null, null, null));
        database.close();
    }

    public void editTileName(View view) {
        final EditText dialogEditText = new EditText(getActivity());
        Cursor cursor = (Cursor) cursorAdapter.getItem(listView.getPositionForView(view));
        final int id = cursor.getInt(cursor.getColumnIndex("_id"));
        dialogEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(80)});
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Zmien nazwe");
        TextView textView = (TextView) view.findViewById(R.id.tileNameTextView);
        dialogEditText.setText(textView.getText());
        builder.setView(dialogEditText);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String name = dialogEditText.getText().toString();
                if (name != null && !name.isEmpty()) {
                    //day.setMonthName(task);
                    updateTileName(id, name);
                    mCallback.OnTileNameAdded();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteTileName(id);
                mCallback.OnTileNameAdded();
            }
        });
        builder.show();
    }

    private void deleteTileName(int id) {
        database = sqLiteOpenHelper.getWritableDatabase();
        String whereClause = "_id=?";
        String[] args = new String[]{Integer.toString(id)};
        int d = database.delete("tilesnames", whereClause, args);
        updateCursor();
        database.close();
    }

    private void updateTileName(int id, String name) {
        database = sqLiteOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        String whereClause = "_id=?";
        String[] args = new String[]{Integer.toString(id)};
        database.update("tilesnames", contentValues, whereClause, args);
        updateCursor();
        database.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                addTileName(view);
                mCallback.OnTileNameAdded();
                break;
        }
    }

    // Interface sluzacy do przekazywania informacji o dodanej tileName do activity
    public interface OnTileNameChangedListener {
        public void OnTileNameAdded();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnTileNameChangedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTileNameChangedListener");
        }

    }
}
