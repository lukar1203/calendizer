package com.calendizer;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Amonite on 19.10.13.
 */
public class DayArrayAdapter extends ArrayAdapter<Day> {
    private Context context;

    public DayArrayAdapter(Context context, int textViewResourceID, ArrayList<Day> items) {
        super(context, textViewResourceID, items);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.day, null);


            ViewHolder holder = new ViewHolder();

            // Numer dnia i miesiac
            holder.dayNumber = (TextView) view.findViewById(R.id.dayNumberTextView);
            holder.month = (TextView) view.findViewById(R.id.monthTextView);

            // Zadania
            holder.task[0] = (TextView) view.findViewById(R.id.task1);
            holder.task[1] = (TextView) view.findViewById(R.id.task2);
            holder.task[2] = (TextView) view.findViewById(R.id.task3);
            holder.task[3] = (TextView) view.findViewById(R.id.task4);
            //holder.addTaskButton = (Button) view.findViewById(R.id.addTaskButton);

            // Kafelki
            holder.tileName[0] = (TextView) view.findViewById(R.id.tileName1);
            holder.tileName[1] = (TextView) view.findViewById(R.id.tileName2);
            holder.tileName[2] = (TextView) view.findViewById(R.id.tileName3);
            holder.tileName[3] = (TextView) view.findViewById(R.id.tileName4);
            holder.tileName[4] = (TextView) view.findViewById(R.id.tileName5);
            holder.tileValue[0] = (TextView) view.findViewById(R.id.tileValue1);
            holder.tileValue[1] = (TextView) view.findViewById(R.id.tileValue2);
            holder.tileValue[2] = (TextView) view.findViewById(R.id.tileValue3);
            holder.tileValue[3] = (TextView) view.findViewById(R.id.tileValue4);
            holder.tileValue[4] = (TextView) view.findViewById(R.id.tileValue5);
            holder.relativeLayout[0] = (RelativeLayout) view.findViewById(R.id.tile1);
            holder.relativeLayout[1] = (RelativeLayout) view.findViewById(R.id.tile2);
            holder.relativeLayout[2] = (RelativeLayout) view.findViewById(R.id.tile3);
            holder.relativeLayout[3] = (RelativeLayout) view.findViewById(R.id.tile4);
            holder.relativeLayout[4] = (RelativeLayout) view.findViewById(R.id.tile5);

            view.setTag(holder);
        }

        Day item = getItem(position);
        if (item != null) {
            ViewHolder holder = (ViewHolder) view.getTag();
            if (holder.dayNumber != null) {
                // ustawia numer dnia
                holder.dayNumber.setText(String.format("" + item.getDayNumber()));
            }
            if (holder.month != null) {
                // ustawia miesiac
                holder.month.setText(String.format(formatMonth(item.getMonthName())));
            }

            //holder.addTaskButton.setVisibility(View.VISIBLE);
            int taskCount = item.getTasks().size();

            for (int i = 0; i < taskCount; i++) {
                if (holder.task[i] != null) {
                    holder.task[i].setText(item.getTasks().get(i).getValue());
                    holder.task[i].setTag(item.getTasks().get(i).getId());
                    holder.task[i].setVisibility(View.VISIBLE);
                }
            }

            // Ukrywa puste zadania
            for (int i = taskCount; i < 4; i++) {
                if (holder.task[i] != null) {
//                    holder.task[i].setVisibility(View.INVISIBLE);
                    holder.task[i].setText(null);
                }
            }
            // Jesli jest dodana maksymalna liczba zadan, to ukrywa przycisk
//            if (taskCount >= 4)
//                holder.addTaskButton.setVisibility(View.INVISIBLE);

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
            int tilesCount = Integer.parseInt(sharedPref.getString("tilesCount", "3"));
            for (int i = 0; i < tilesCount; i++) {
                holder.tileName[i].setText(sharedPref.getString("tile_name_" + (i + 1), ""));
                holder.relativeLayout[i].setVisibility(View.VISIBLE);
            }

            for (int i = tilesCount; i < 5; i++) {
                String string = sharedPref.getString("tile_name_" + (i + 1), "");
                holder.tileName[i].setText(string);
                holder.relativeLayout[i].setVisibility(View.GONE);
            }

            for (int i = 0; i < 5; i++) {
                int value = item.getTiles()[i].getValue();
                holder.tileValue[i].setText(Integer.toString(value));
            }
        }

        return view;
    }

    public String formatMonth(int month) {
        DateFormat formatter = new SimpleDateFormat("MMM");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MONTH, month);
        return formatter.format(calendar.getTime());
    }

    static class ViewHolder {
        TextView dayNumber;
        TextView month;
        TextView task[] = new TextView[4];
        //Button addTaskButton;
        TextView tileName[] = new TextView[5];
        TextView tileValue[] = new TextView[5];
        RelativeLayout relativeLayout[] = new RelativeLayout[5];
    }
}
