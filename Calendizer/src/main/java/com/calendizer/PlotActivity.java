package com.calendizer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.TextView;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.lang.reflect.Array;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//public class PlotActivity extends Activity {
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.plot_main);
//
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }
//    }
//package com.androidplot.demos;

/**
 * A straightforward example of using AndroidPlot to plot some data.
 */
public class PlotActivity extends CoreActivity {

    private XYPlot plot;

    private SQLiteDatabase database;
    private CharSequence[] entries;
    private int[][] numbers;
    private final int maxTilesCount = 5;
    private CalendizerSQLiteOpenHelper sqLiteOpenHelper;
    private String[] dates;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int tilePosition = 0, czyMiesieczny = 1;
        String tileName;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tilePosition = extras.getInt("TILEPOSITION");
            czyMiesieczny = extras.getInt("CZYMIESIECZNY");
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        tileName = sharedPref.getString("tile_name_" + (tilePosition + 1), "");

        String podpis;
        if (czyMiesieczny==1)
            podpis = tileName + " - wykres miesieczny";
        else
            podpis = tileName + " - wykres roczny";
        podpis = tileName;

        // fun little snippet that prevents users from taking screenshots
        // on ICS+ devices :-)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.simple_xy_plot_example);

        // initialize our XYPlot reference:
        plot = (XYPlot) findViewById(R.id.mySimpleXYPlot);

        // Create a couple arrays of y-values to plot:
        //Number[] series1Numbers = {1, 8, 5, 2, 7, 4};
        //Number[] series2Numbers = {4, 6, 3, 8, 2, 10};
        int[] kk = loadTilesNumbers(tilePosition);
        List<Integer> series3List = new ArrayList<Integer>();
        for (int i : kk) series3List.add(i);
        //val javaArray: Array[java.lang.Integer] = scalaArray map Integer.valueOf;



        // Turn the above arrays into XYSeries':
        XYSeries series1 = new SimpleXYSeries(
                //Arrays.asList(series1Numbers),          // SimpleXYSeries takes a List so turn our array into a List
                series3List,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, // Y_VALS_ONLY means use the element index as the x value
                podpis);                             // Set the display title of the series

        // same as above
//        XYSeries series2 = new SimpleXYSeries(Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Series2");

        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml:
        LineAndPointFormatter series1Format = new LineAndPointFormatter();
        series1Format.setPointLabelFormatter(new PointLabelFormatter());
        series1Format.configure(getApplicationContext(),
                R.xml.line_point_formatter_with_plf1);

        // add a new series' to the xyplot:
        plot.addSeries(series1, series1Format);

        // same as above:
//        LineAndPointFormatter series2Format = new LineAndPointFormatter();
//        series2Format.setPointLabelFormatter(new PointLabelFormatter());
//        series2Format.configure(getApplicationContext(),
//                R.xml.line_point_formatter_with_plf2);
//        plot.addSeries(series2, series2Format);

        // reduce the number of range labels
        plot.setTicksPerRangeLabel(1);
        //plot.setTicksPerDomainLabel(2);
        plot.getGraphWidget().setDomainLabelOrientation(-45);

        MyIndexFormat mif = new MyIndexFormat ();
        String[] jj = loadTilesDates(tilePosition);
        mif.Labels = jj;//new String[]{"label1", "label2", "i 3"};// TODO: fill the array with your custom labels

        // attach index->string formatter to the plot instance
        plot.getGraphWidget().setDomainValueFormat(mif);

    }

    public int[] loadTilesNumbers(int position) {
        //String[] columns = {"_id", "name"};
        //Cursor cursor = database.query("tilesnames", columns, null, null, null, null, null);
        //Cursor cursor = database.rawQuery("Select * from tilesnames", null);
        sqLiteOpenHelper = new CalendizerSQLiteOpenHelper(this);
        database = sqLiteOpenHelper.getReadableDatabase();
        String[] columns = {"_id", "value", "date"};
        String selection = "position=?";
        String[] selectionArgs = {Integer.toString(position)};
        Cursor cursor = database.query("tiles", columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        if(count==0) {int[] zero={0}; return zero;};
        cursor.moveToFirst();
        int[] numbers = new int[count];
        int min,max,sum=0,curV;
        double avg=0;
        min=max=cursor.getInt(cursor.getColumnIndex("value"));
        for (int i = 0; i < count; i++) {
            numbers[i] = cursor.getInt(cursor.getColumnIndex("value"));
            //dates[i] = cursor.getString(cursor.getColumnIndex("date"));
//            if (curV>max)
//                max=curV;
//            if (curV<min)
//                min=curV;
//            sum+=curV;
            cursor.moveToNext();
        }
 //       avg = 1.0d * sum/count;

        cursor.close();
        database.close();
        return numbers;
    }
    public String[] loadTilesDates(int position) {
        //String[] columns = {"_id", "name"};
        //Cursor cursor = database.query("tilesnames", columns, null, null, null, null, null);
        //Cursor cursor = database.rawQuery("Select * from tilesnames", null);
        sqLiteOpenHelper = new CalendizerSQLiteOpenHelper(this);
        database = sqLiteOpenHelper.getReadableDatabase();
        String[] columns = {"_id", "value", "date"};
        String selection = "position=?";
        String[] selectionArgs = {Integer.toString(position)};
        Cursor cursor = database.query("tiles", columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        if(count==0) {String[] zero={""}; return zero;};
        cursor.moveToFirst();
        int[] numbers = new int[count];
        String[] dates = new String[count];
        int min,max,sum=0,curV;
        double avg=0;
        min=max=cursor.getInt(cursor.getColumnIndex("value"));
        for (int i = 0; i < count; i++) {
            //numbers[i] = cursor.getInt(cursor.getColumnIndex("value"));
            dates[i] = cursor.getString(cursor.getColumnIndex("date"));
            //miesiac zwiekszam o 1:
            String[] arr = dates[i].split("[-]");
            int mies = Integer.parseInt(arr[1]);
            mies++;
            dates[i] = arr[0]+"-"+Integer.toString(mies)+"-"+arr[2];

//            if (curV>max)
//                max=curV;
//            if (curV<min)
//                min=curV;
//            sum+=curV;
            cursor.moveToNext();
        }
 //       avg = 1.0d * sum/count;

        cursor.close();
        database.close();
        return dates;
    }

    public class MyIndexFormat extends Format {

        public String[] Labels = null;

        @Override
        public Object parseObject(String source, ParsePosition pos) {
            return null;
        }

        @Override
        public StringBuffer format(Object obj,
                                   StringBuffer toAppendTo,
                                   FieldPosition pos) {

            // try turning value to index because it comes from indexes
            // but if is too far from index, ignore it - it is a tick between indexes
            float fl = ((Number)obj).floatValue();
            int index = Math.round(fl);
            if(Labels == null || Labels.length <= index ||
                    Math.abs(fl - index) > 0.1)
                return new StringBuffer("");

            return new StringBuffer(Labels[index]);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //menu.removeItem(R.id.action_plotView);
        return true;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }

//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            return rootView;
//        }
//    }

}
