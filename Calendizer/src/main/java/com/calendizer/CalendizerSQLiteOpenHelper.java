package com.calendizer;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Amonite on 24.10.13.
 */
public class CalendizerSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Calendizer";
    private static final String TASKS_TABLE_NAME = "tasks";
    private static final String TILES_TABLE_NAME = "tiles";
    private static final String TILESNAMES_TABLE = "tilesnames";
    private static final String TILESVIEWED_TABLE = "tilesviewed";
    private static final String TILESCOUNT_TABLE = "tilescount";
    private static final String TASKS_TABLE_CREATE = "CREATE TABLE " + TASKS_TABLE_NAME + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT, date TEXT );";
    private static final String TILESNAMES_TABLE_CREATE = "CREATE TABLE " + TILESNAMES_TABLE + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);";
    private static final String TILES_TABLE_CREATE = "CREATE TABLE " + TILES_TABLE_NAME + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, value INTEGER, position INTEGER, date TEXT );";
    private static final String TILES_VIEWED_CREATE = "CREATE TABLE " + TILESVIEWED_TABLE + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, position INTEGER, name TEXT );";
    private static final String TILESCOUNT_TABLE_CREATE = "CREATE TABLE " + TILESCOUNT_TABLE + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, count INTEGER);";

    public CalendizerSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TASKS_TABLE_CREATE);
        sqLiteDatabase.execSQL(TILES_TABLE_CREATE);
        sqLiteDatabase.execSQL(TILESNAMES_TABLE_CREATE);
        sqLiteDatabase.execSQL(TILES_VIEWED_CREATE);
        sqLiteDatabase.execSQL(TILESCOUNT_TABLE_CREATE);
        insertDefaultTilesNames(sqLiteDatabase);
        insertDefaultTilesViewed(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TASKS_TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TILES_TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TILESNAMES_TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TILESVIEWED_TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TILESCOUNT_TABLE);
    }

    private void insertDefaultTilesNames(SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "bieganie");
        sqLiteDatabase.insert("tilesnames", null, contentValues);
        contentValues = new ContentValues();
        contentValues.put("name", "pływanie");
        sqLiteDatabase.insert("tilesnames", null, contentValues);
        contentValues = new ContentValues();
        contentValues.put("name", "rower");
        sqLiteDatabase.insert("tilesnames", null, contentValues);
    }

    private void insertDefaultTilesViewed(SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "bieganie");
        contentValues.put("position", 1);
        sqLiteDatabase.insert("tilesviewed", null, contentValues);
        contentValues.put("name", "pływanie");
        contentValues.put("position", 2);
        sqLiteDatabase.insert("tilesviewed", null, contentValues);
        contentValues.put("name", "rower");
        contentValues.put("position", 3);
        sqLiteDatabase.insert("tilesviewed", null, contentValues);
        contentValues.put("name", "4");
        contentValues.put("position", 4);
        sqLiteDatabase.insert("tilesviewed", null, contentValues);
        contentValues.put("name", "5");
        contentValues.put("position", 5);
        sqLiteDatabase.insert("tilesviewed", null, contentValues);
        contentValues.clear();
        contentValues.put("count", 3);
        sqLiteDatabase.insert("tilescount", null, contentValues);
    }
}
