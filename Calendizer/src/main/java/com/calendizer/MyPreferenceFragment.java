package com.calendizer;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;

/**
 * Created by Amonite on 15.11.13.
 */
public class MyPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private CharSequence[] entries;
    private final int maxTilesCount = 5;
    private CalendizerSQLiteOpenHelper sqLiteOpenHelper;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        sqLiteOpenHelper = new CalendizerSQLiteOpenHelper(getActivity());
        setPreferences();

        setDownloadedSummaries();

        this.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);


    }

    public void setPreferences() {
        entries = loadTilesNames();
        for (int i = 1; i <= maxTilesCount; i++) {
            String key = "tile_name_" + i;
            ListPreference listPreference = (ListPreference) findPreference(key);
            listPreference.setEntries(entries);
            listPreference.setEntryValues(entries);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
        this.initSummaries(this.getPreferenceScreen());
        //setDownloadedSummaries();
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference connectionPref = findPreference(key);
        connectionPref.setSummary(sharedPreferences.getString(key, ""));
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        if (Character.isDigit(key.charAt(key.length() - 1))) {

            String whereClause = "position=?";
            String s = Character.toString(key.charAt(key.length() - 1));
            int val = Integer.parseInt(s) - 1;
            String[] args = new String[]{Integer.toString(val)};
            int d = db.delete("tiles", whereClause, args);

            String name = connectionPref.getSummary().toString();

            ContentValues contentValues = new ContentValues();
            contentValues.put("name", name);
            whereClause = "position=?";
            args = new String[]{Integer.toString(val + 1)};
            db.update("tilesviewed", contentValues, whereClause, args);
        } else {
            ContentValues contentValues = new ContentValues();
            String count = ((ListPreference) connectionPref).getValue();
            contentValues.put("count", count);
            db.update("tilescount", contentValues, null, null);
        }

        db.close();
    }

    public void setDownloadedSummaries(){
        String key;
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();
        String[] columns = {"name", "position"};
        Cursor cursor = db.query("tilesviewed", columns, null, null, null, null, "position");
        cursor.moveToFirst();
        String names[] = new String[5];
        for (int i = 0; i < cursor.getCount(); i++) {
            int pos = cursor.getInt(cursor.getColumnIndex("position"));
            names[i] = cursor.getString(cursor.getColumnIndex("name"));
            cursor.moveToNext();
        }

        for (int i = 1; i<=5; i++){
            key="tile_name_"+i;
            ListPreference connectionPref = (ListPreference) findPreference(key);
            connectionPref.setSummary(names[i-1]);
            connectionPref.setValue(names[i - 1]);
            connectionPref.setPersistent(true);
        }
        // ((BaseAdapter) this.getPreferenceScreen().getRootAdapter()).notifyDataSetChanged();
        //this.initSummaries(this.getPreferenceScreen());

        cursor = db.query("tilescount", new String[]{"count"}, null, null, null, null, null);
        cursor.moveToFirst();
        ListPreference connectionPref = (ListPreference) findPreference("tilesCount");
        connectionPref.setValue(Integer.toString(cursor.getInt(cursor.getColumnIndex("count"))));
        connectionPref.setPersistent(true);

        cursor.close();
        db.close();
    }



    private void initSummaries(PreferenceGroup pg) {
        for (int i = 0; i < pg.getPreferenceCount(); ++i) {
            Preference p = pg.getPreference(i);
            if (p instanceof PreferenceGroup)
                this.initSummaries((PreferenceGroup) p);
            else
                this.setSummary(p);
        }
    }

    private void setSummary(Preference pref) {
        // react on type or key
        if (pref instanceof ListPreference) {
            ListPreference listPref = (ListPreference) pref;
            pref.setSummary(listPref.getEntry());
            pref.setPersistent(true);
        }
    }

    public CharSequence[] loadTilesNames() {
        SQLiteDatabase database = sqLiteOpenHelper.getWritableDatabase();
        String[] columns = {"_id", "name"};
        Cursor cursor = database.query("tilesnames", columns, null, null, null, null, null);
        //Cursor cursor = database.rawQuery("Select * from tilesnames", null);
        cursor.moveToFirst();
        int count = cursor.getCount();
        CharSequence[] names = new CharSequence[count];
        for (int i = 0; i < count; i++) {
            names[i] = cursor.getString(cursor.getColumnIndex("name"));
            cursor.moveToNext();
        }

        database.close();
        return names;
    }
}
